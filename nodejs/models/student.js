const mongoose = require('mongoose');

var Student = mongoose.model('student',{
    name : {type : String },
    email : {type : String },
    mobile : {type : String },
    address : {type : String }

});

module.exports = {
    Student
};
