import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StudentService } from '../shared/student.service';
import { Student } from '../shared/student.model';
import { NgForm } from '@angular/forms';

declare var M : any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers : [StudentService]

})
export class DashboardComponent implements OnInit {


  studentList:any;
  constructor(private router: Router,public studentService:StudentService) {
    this.getAllData();
   }

  ngOnInit(): void { 
    this.resetForm();
    this.refreshStudentList();
  }

  openRegister(){
    this.router.navigateByUrl('/register');
  }

  getAllData(){
    this.studentService.getStudentList().subscribe(res=>{
      this.studentList=res;
      
    });
  }

  

  onEdit(id){
    this.studentService.selectedStudent = id;
    this.router.navigateByUrl('/register/'+id);
      
      M.toast({html : 'Updated Successfully',classes : 'rounded' });

  }

  resetForm(form? : NgForm){
    if(form)
    form.reset();
    this.studentService.selectedStudent = {
      _id: "",
      name:"",
      email:"",
      mobile:"",
      address:"",
    }
  }

  refreshStudentList(){
    this.studentService.getStudentList().subscribe((res)=>{
      this.studentList = res;
    });
  }  
  
  onDelete(_id : string, form : NgForm){
    if(confirm('Are you sure to delete this record ?')==true){
      this.studentService.deleteStudent(_id).subscribe((res)=>{
        this.refreshStudentList();
        M.toast({html : 'Deleted Successfully',classes : 'rounded' });
      });
    }

  }
}
