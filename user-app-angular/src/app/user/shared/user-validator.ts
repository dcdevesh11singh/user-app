import { AbstractControl } from "@angular/forms";

export function nameValidator(control:AbstractControl) :{[key : string] : any} | null {
    const val = /admin/.test(control.value);
    return val ? {'customError' : {value:control.value}} : null;

}

