export class Student {
    _id : string;
    name : string;
    email : string;
    mobile : string;
    address : string;
}
