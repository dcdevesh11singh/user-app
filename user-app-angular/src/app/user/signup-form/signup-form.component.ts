import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Student } from '../shared/student.model';
import { StudentService } from '../shared/student.service';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { nameValidator } from '../shared/user-validator';
// import {Router} from '@angular/router'

declare var M : any;

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css'],
  providers : [StudentService]
})
export class SignupFormComponent implements OnInit {
  // signupForm : FormGroup;

  constructor(public studentService : StudentService) { }
  // constructor(private formBuilder : FormBuilder,private router : Router) { }

  ngOnInit(): void {
    this.resetForm();
    console.log("devesh");

    // this.signupForm = this.formBuilder.group({
      // name:['',Validators.required],
      // email:['',Validators.required],
      // mobile:['',Validators.required],
      // address:['',Validators.required]

    // });
  }
    
  // nameValidation(){
  //   let name = this.signupForm.controls.name.value;
  //   console.log(name);
  // }

  resetForm(form? : NgForm){
    if(form)
    form.reset();
    this.studentService.selectedStudent = {
      _id: "",
      name:"",
      email:"",
      mobile:"",
      address:"",
    }
    
  }
  onSubmit(form : NgForm){
    if(form.value._id=="" || form.value._id=="null" || form.value._id==null){
    this.studentService.postStudent(form.value).subscribe((res)=>{
      M.toast({html : 'Saved Successfully',classes : 'rounded' });
      // this.router.navigateByUrl('/dashboard');
      
    });
  }
  }
  }

  
