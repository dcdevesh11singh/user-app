import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

declare var M : any;

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder,private router: Router) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

   username:string = "dcdevesh11@gmail.com"; 
   password:string = "123";


   login(){
    let nameInputVal = this.loginForm.controls.email.value;
    let passwordInputVal = this.loginForm.controls.password.value;
    console.log(nameInputVal);
    if(this.username===nameInputVal && this.password===passwordInputVal){
       console.log("valid user");
       this.router.navigateByUrl('/dashboard');
    }
    else{
      console.log("invalid user");
      M.toast({html : 'Invalid user',classes : 'rounded' });
    }
   }

}
