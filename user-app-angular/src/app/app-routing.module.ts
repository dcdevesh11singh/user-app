import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './user/dashboard/dashboard.component';
//routing
import { LoginFormComponent } from './user/login-form/login-form.component';
import { SignupFormComponent } from './user/signup-form/signup-form.component';
 

const routes: Routes = [
  { path : '', component : LoginFormComponent },
  { path : 'register', component : SignupFormComponent }, 
  {path : 'dashboard',component : DashboardComponent},
  {path : 'register/:id',component : SignupFormComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers : []
})
export class AppRoutingModule { }
